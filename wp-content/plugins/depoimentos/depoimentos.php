<?php

/*
Plugin Name: Depoimentos Uhu
Plugin URL: http://www.unius.com.br
Description: Plugin for custom post type from Uhu to create Servico
Version: 1.0
Author: Felipe Alves
License: GPLv2
*/

function unius_custom_post_depoimentos() {
  $labels = array(
    'name'               => _x( 'Depoimentos', 'post type general name' ),
    'singular_name'      => _x( 'Depoimentos', 'post type singular name' ),
    'add_new'            => _x( 'Adicionar Novo', 'book' ),
    'add_new_item'       => __( 'Adicionar Novo depoimentos' ),
    'edit_item'          => __( 'Editar depoimentos' ),
    'new_item'           => __( 'Novo depoimentos' ),
    'all_items'          => __( 'Todos depoimentos' ),
    'view_item'          => __( 'Visualizar depoimentos' ),
    'search_items'       => __( 'Buscar depoimentos' ),
    'not_found'          => __( 'Nenhum depoimentos encontrado' ),
    'not_found_in_trash' => __( 'Não há depoimentos na lixeira' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Depoimentos'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Cadastro de depoimentos',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array('title','editor','comments','thumbnail','excerpt'),
    'has_archive'   => true,
    'menu_icon'     => 'dashicons-format-status',
  );
  register_post_type( 'depoimentos', $args );
}
add_action( 'init', 'unius_custom_post_depoimentos' );

function depoimentos_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['depoimentos'] = array(
    0 => '',
    1 => sprintf( __('Depoimentos Atualizado. <a href="%s" target="_blank">Visualizar depoimentos</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Depoimentos atualizado.'),
    5 => isset($_GET['revision']) ? sprintf( __('Evento restaurado revisado por %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Depoimentos publicado. <a href="%s" target="_blank">Visualizar Evento</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Serviço salvo.'),
    8 => sprintf( __('Depoimentos enviado. <a target="_blank" href="%s">Pré-visualização</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Depoimentos programado para: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Pré-Visualizar distributor</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Depoimentos rascunho atualizado. <a href="%s" target="_blank">Pré Visualizar evento</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'depoimentos_updated_messages' );

function depoimentos_contextual_help( $contextual_help, $screen_id, $screen ) {
  if ( 'depoimentos' == $screen->id ) {

    $contextual_help = '<h2>Depoimentos</h2>
    <p>Depoimentos utilizado para cadastro de depoimentos que serão exibidos no bloco Depoimentos.</p>
    <p>Aqui é possivel editar/vizualiar os depoimentos.</p>';

  } elseif ( 'edit-depoimentos' == $screen->id ) {

    $contextual_help = '<h2>Editando depoimentos</h2>
    <p>Esta página é utilizada para edição/ visualização de depoimentos.</p>';

  }
  return $contextual_help;
}
add_action( 'contextual_help', 'depoimentos_contextual_help', 10, 3 );

function depoimento_taxonomies_depoimento() {
  $labels = array(
    'name'              => _x( 'Categorias de depoimento', 'taxonomy general name' ),
    'singular_name'     => _x( 'Categoria depoimento', 'taxonomy singular name' ),
    'search_items'      => __( 'Busca por categorias de depoimento' ),
    'all_items'         => __( 'Todas as categorias' ),
    'parent_item'       => __( 'Categoria relacionada para depoimento' ),
    'parent_item_colon' => __( 'Categoria relacionada para depoimento:' ),
    'edit_item'         => __( 'Editar categoria do depoimento' ),
    'update_item'       => __( 'Atualizar Categoria de depoimento' ),
    'add_new_item'      => __( 'Adicionar nova categoria no depoimento' ),
    'new_item_name'     => __( 'Nova categoria de depoimento' ),
    'menu_name'         => __( 'Categorias depoimento' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'depoimento', 'depoimentos', $args );
}
add_action( 'init', 'depoimento_taxonomies_depoimento', 0 );

if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'post-thumbnails' );
}