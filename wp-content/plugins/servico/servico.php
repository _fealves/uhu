<?php

/*
Plugin Name: Servicos Uhu
Plugin URL: http://www.unius.com.br
Description: Plugin for custom post type from Uhu to create Servico
Version: 1.0
Author: Felipe Alves
License: GPLv2
*/

function unius_custom_post_servicos() {
  $labels = array(
    'name'               => _x( 'Serviços', 'post type general name' ),
    'singular_name'      => _x( 'Serviços', 'post type singular name' ),
    'add_new'            => _x( 'Adicionar Novo', 'book' ),
    'add_new_item'       => __( 'Adicionar Novo Serviços' ),
    'edit_item'          => __( 'Editar Serviços' ),
    'new_item'           => __( 'Novo Serviços' ),
    'all_items'          => __( 'Todos Serviços' ),
    'view_item'          => __( 'Visualizar Serviços' ),
    'search_items'       => __( 'Buscar Serviços' ),
    'not_found'          => __( 'Nenhum serviços encontrado' ),
    'not_found_in_trash' => __( 'Não há serviços na lixeira' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Serviços'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Cadastro de serviços',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array('title','editor','comments','thumbnail','excerpt'),
    'has_archive'   => true,
    'menu_icon'     => 'dashicons-hammer',
  );
  register_post_type( 'servico', $args );
}
add_action( 'init', 'unius_custom_post_servicos' );

function servicos_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['servico'] = array(
    0 => '',
    1 => sprintf( __('Serviço Atualizado. <a href="%s" target="_blank">Visualizar Serviços</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Serviço atualizado.'),
    5 => isset($_GET['revision']) ? sprintf( __('Evento restaurado revisado por %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Serviço publicado. <a href="%s" target="_blank">Visualizar Evento</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Serviço salvo.'),
    8 => sprintf( __('Serviço enviado. <a target="_blank" href="%s">Pré-visualização</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Serviço programado para: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Pré-Visualizar distributor</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Serviço rascunho atualizado. <a href="%s" target="_blank">Pré Visualizar evento</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'servicos_updated_messages' );

function servicos_contextual_help( $contextual_help, $screen_id, $screen ) {
  if ( 'servico' == $screen->id ) {

    $contextual_help = '<h2>Serviços</h2>
    <p>Serviço utilizado para cadastro de serviços que serão exibidos na home.</p>
    <p>Aqui é possivel editar/vizualiar os serviços.</p>';

  } elseif ( 'edit-servico' == $screen->id ) {

    $contextual_help = '<h2>Editando serviços</h2>
    <p>Esta página é utilizada para edição/ visualização de serviços.</p>';

  }
  return $contextual_help;
}
add_action( 'contextual_help', 'servicos_contextual_help', 10, 3 );

function servico_taxonomies_servico() {
  $labels = array(
    'name'              => _x( 'Categorias de serviço', 'taxonomy general name' ),
    'singular_name'     => _x( 'Categoria serviço', 'taxonomy singular name' ),
    'search_items'      => __( 'Busca por categorias de serviço' ),
    'all_items'         => __( 'Todas as categorias' ),
    'parent_item'       => __( 'Categoria relacionada para serviço' ),
    'parent_item_colon' => __( 'Categoria relacionada para serviço:' ),
    'edit_item'         => __( 'Editar categoria do serviço' ),
    'update_item'       => __( 'Atualizar Categoria de serviço' ),
    'add_new_item'      => __( 'Adicionar nova categoria no serviço' ),
    'new_item_name'     => __( 'Nova categoria de serviço' ),
    'menu_name'         => __( 'Categorias serviços' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'servicos', 'servico', $args );
}
add_action( 'init', 'servico_taxonomies_servico', 0 );

if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'post-thumbnails' );
}