<?php get_header(); ?>
<div class="container-fluid">
    <section id="page-contato">
        <div class="row">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 title-contato">
                        <h1><?php the_title()?></h1>
                        <p>Quer contratar nossos serviços? Têm dúvidas, sugestões ou quer conversar com a gente? Ligue ou mande uma mensagem. Vamos adorar ;) </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 localizacao">

                        <div class="col-lg-4 col-md-4 col-ls-4 col-xs-12">
                            <img src="<?php bloginfo('url')?>/wp-content/upload/tel.jpg" title="Localização" alt="Localização">
                            <p><?php the_field('localizacao');?></p>
                        </div>

                        <div class="col-lg-4 col-md-4 col-ls-4 col-xs-12">
                            <img src="<?php bloginfo('url')?>/wp-content/upload/tel.jpg" title="Telefone" alt="Telefone">
                            <p><?php the_field('tel');?></p>
                        </div>

                        <div class="col-lg-4 col-md-4 col-ls-4 col-xs-12">
                            <img src="<?php bloginfo('url')?>/wp-content/upload/mail.jpg" title="E-mail" alt="E-mail">
                            <p><?php the_field('email');?></p>
                        </div>

                    </div>
                </div>

                <div class="row" id="form-localizacao">
                    <div class="col-lg-6 form">
                        <?php echo do_shortcode(get_field('form_contato'))?>
                    </div>

                    <div class="col-lg-6 map-localizacao">
                        <div class="col-lg-12">
                            <h2>Localização</h2>
                        </div>
                        <div class="col-lg-12">
                            <?php /*the_field('mapa')*/?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="redes-sociais">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-ls-2 col-xs-12"></div>
                    <div class="col-lg-8 col-md-8 col-ls-8 col-ls-12 block_social">
                        <div class="row">
                            <div class="col-lg-12">
                                <p><b>Acompanhe nossas redes sociais</b></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 social-contato-orc">
                                <div class="container-redes-sociais">
                                    <div class="col-lg-3 col-md-3 col-ls-3 col-xs-3"><a href="https://www.facebook.com/Uhu-a-gente-cuida-962838803799629/" target="_blank"><img src="<?php bloginfo('url')?>/wp-content/upload/facebook-b.png" alt=""></a></div>

                                    <div class="col-lg-3 col-md-3 col-ls-3 col-xs-3"><a href="https://www.linkedin.com/company/10560060?trk=tyah&trkInfo=clickedVertical%3Acompany%2CentityType%3AentityHistoryName%2CclickedEntityId%3Acompany_10560060%2Cidx%3A0" target="_blank"><img src="<?php bloginfo('url')?>/wp-content/upload/in-b.png" alt=""></a></div>

                                    <div class="col-lg-3 col-md-3 col-ls-3 col-xs-3"><a href="https://www.instagram.com/uhu_agentecuida/" target="_blank"><img src="<?php bloginfo('url')?>/wp-content/upload/insta-b.png" alt=""></a></div>

                                    <div class="col-lg-3 col-md-3 col-ls-3 col-xs-3"><a href="https://twitter.com/uhu_agentecuida" target="_blank"><img src="<?php bloginfo('url')?>/wp-content/upload/twitter-b.png" alt=""></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-ls-2 col-xs-12"></div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php get_footer(); ?>
