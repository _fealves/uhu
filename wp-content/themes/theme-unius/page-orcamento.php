<?php get_header(); ?>
<div class="container-fluid">
    <section id="page-contato">
        <div class="row">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 title-contato">
                        <h1><?php the_title()?></h1>
                        <p>Aliquam dapibus massa diam, eu porta ipsum mollis vel. Etiam blandit auctor porta.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 localizacao">

                        <div class="col-lg-4 col-md-4 col-ls-4 col-xs-12">
                            <img src="<?php bloginfo('url')?>/wp-content/upload/tel.jpg" title="Localização" alt="Localização">
                            <p><?php the_field('localizacao');?></p>
                        </div>

                        <div class="col-lg-4 col-md-4 col-ls-4 col-xs-12">
                            <img src="<?php bloginfo('url')?>/wp-content/upload/tel.jpg" title="Telefone" alt="Telefone">
                            <p><?php the_field('tel');?></p>
                        </div>

                        <div class="col-lg-4 col-md-4 col-ls-4 col-xs-12">
                            <img src="<?php bloginfo('url')?>/wp-content/upload/mail.jpg" title="E-mail" alt="E-mail">
                            <p><?php the_field('email');?></p>
                        </div>

                    </div>
                </div>

                <div id="form-localizacao">
                    <?php echo do_shortcode(get_field('form_contato'));?>
                </div>
            </div>
        </div>
        <div class="row" id="redes-sociais">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-ls-2 col-xs-12"></div>
                    <div class="col-lg-8 col-md-8 col-ls-8 col-ls-12 block_social">
                        <div class="row">
                            <div class="col-lg-12">
                                <p><b>Acompanhe nossas redes sociais</b></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 social-contato-orc">
                                <div class="container-redes-sociais">
                                    <div class="col-lg-3 col-md-3 col-ls-3 col-xs-3"><a href=""><img src="<?php bloginfo('url')?>/wp-content/upload/facebook-b.png" alt=""></a></div>

                                    <div class="col-lg-3 col-md-3 col-ls-3 col-xs-3"><a href=""><img src="<?php bloginfo('url')?>/wp-content/upload/in-b.png" alt=""></a></div>

                                    <div class="col-lg-3 col-md-3 col-ls-3 col-xs-3"><a href=""><img src="<?php bloginfo('url')?>/wp-content/upload/insta-b.png" alt=""></a></div>

                                    <div class="col-lg-3 col-md-3 col-ls-3 col-xs-3"><a href=""><img src="<?php bloginfo('url')?>/wp-content/upload/twitter-b.png" alt=""></a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-ls-2 col-xs-12"></div>
            </div>
        </div>
    </section>
</div>

<?php get_footer(); ?>
