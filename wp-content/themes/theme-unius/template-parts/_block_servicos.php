<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <p class="textoServico">Conheça nossos serviços</p>
        </div>

        <div class="col-lg-12 col-md-12">
            <h2>Limpeza</h2>

            <div id="slider-limpeza">

                <?php
                $limpeza = get_term_by('slug','limpeza','servicos');
                $categoria = get_term($limpeza,'servicos');


                $args = array(
                    'post_type'  => 'servico',
                    'order'   => 'ASC',
                    'posts_per_page' => 8,
                    'tax_query' => array(
                        array(
                            'taxonomy'          => 'servicos',
                            'field'             => 'term_id',
                            'terms'             => $categoria->term_id
                        ),
                    ),
                );

                $servicoLimpeza = new WP_Query($args);
                echo $cat->name;
                if($servicoLimpeza->have_posts()){
                    while($servicoLimpeza->have_posts()){
                        $servicoLimpeza->the_post();
                        ?>
                        <figure class="limpeza">
                            <div class="item-limpeza">
                                <a href="<?php echo get_permalink($query->ID)?>" class="hover-limpeza">
                                    <img  src="<?php the_field('img_principal')?>" alt="" style="" class="img-principal">
                                    <img  src="<?php the_field('img_hover')?>" alt="" class="img-hover">
                                </a>
                            </div>
                        </figure>
                        <?php
                    }
                    wp_reset_postdata();
                }

                ?>

            </div>
        </div>

        <div class="col-lg-12 col-md-12">

        </div>

        <div class="col-lg-12 col-md-12 servico-organizacao">
            <h2>Organização</h2>

            <div id="slider-organizacao">

                <?php
                $limpeza = get_term_by('slug','organizacao','servicos');
                $categoria = get_term($limpeza,'servicos');


                $args = array(
                    'post_type'  => 'servico',
                    'posts_per_page' => 8,
                    'tax_query' => array(
                        array(
                            'taxonomy'          => 'servicos',
                            'field'             => 'term_id',
                            'terms'             => $categoria->term_id
                        ),
                    ),
                );

                $query = new WP_Query($args);
                echo $cat->name;
                if($query->have_posts()){
                    while($query->have_posts()){
                        $query->the_post();
                        ?>

                        <figure class="organizacao">
                            <div class="item-organizacao">
                                <a href="<?php echo get_permalink($query->ID)?>" class="hover-limpeza">
                                    <img src="<?php the_field('img_principal')?>" alt="" style="" class="img-principal">
                                    <img src="<?php the_field('img_hover')?>" alt="" class="img-hover">
                                </a>
                            </div>
                        </figure>
                        <?php
                    }
                    wp_reset_postdata();
                }

                ?>

            </div>
        </div>
    </div>
</div>