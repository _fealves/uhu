<?php
/**
* The template part for displaying content of banner from Skinscience
*
* This is the template that displays all banner content by default.
*
* @package WordPress
* @subpackage Theme_Unius
* @since Theme Unius 1.0
*/
?>

<div class="container-fluid">
    <div class="banner">
    	<?php if(is_home()) :?>
			<?php
				$args = array(
					 'posts_per_page' => 8,
					 'orderby' => 'rand',
					 'post_type' => 'banner',
					 'banner_category' => 'institucional',
					 'post_status' => 'publish'
				);
				$show_banner = get_posts( $args );
			?>
			<div class="bxslider">
				<?php foreach ( $show_banner as $post ):?>
						<?php $img_banner = get_field('imagem_banner') ?>
						<div>
							<img src="<?php echo $img_banner['url'];?>">
						</div>
				<?php endforeach ?> 
			</div>
		<?php endif?>
    </div>
</div>
