<div class="container depoimentos">
    <div class="row depoimentos-border">
        <div class="col-lg-12 block-depoimentos">
            <div class="col-lg-5 col-md-5 depoimento-full">
                <div class="col-lg-2 col-md-2">
                    <p class="depoimento-aspas">“</p>
                    <div class="barra-azul"></div>
                </div>
                <div class="col-lg-10 col-md-10">
                    <h2>Depoi<br>men<br>tos</h2>
                </div>
            </div>

            <div class="col-ls-12 col-xs-12 depoimento-min">
                <h2>“ Depoimentos</h2>
            </div>

            <div class="col-lg-7 col-md-7 col-ls-12 col-xs-12">
                <div class="caixa-rosa">
                    <?php

                    $depoimentos = get_term_by('slug','depoimentos','depoimento');
                    $categoria = get_term($depoimentos,'depoimento');
                    
                    $args = array(
                    'post_type'  => 'depoimentos',
                    'order'   => 'ASC',
                    'tax_query' => array(
                    array(
                    'taxonomy'          => 'depoimento',
                    'field'             => 'term_id',
                    'terms'             => $categoria->term_id
                    ),
                    ),
                    );

                    $depoimentosBlock = new WP_Query($args);
                    echo $cat->name;
                    if($depoimentosBlock->have_posts()){
                    while($depoimentosBlock->have_posts()){
                        $depoimentosBlock->the_post();
                    ?>
                        <div class="slider-depoimento">
                            <div class="content-depoimentos">
                                <p><?php the_content();?></p>
                                <p class="depoimento-autor"><?php the_field('autor')?></p>
                            </div>
                        </div>
                    <?php
                    }
                    wp_reset_postdata();
                    }

                    ?>


                </div>

                <a href="/contato"><button>Envie seu depoimento</button></a>
            </div>
        </div>
    </div>
</div>