<?php get_header(); ?>
<section id="page-404">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 content-404">
                <p class="rosa">404 - PÁGINA NÃO ENCONTRADA</p>
                <p class="azul">PARECE QUE NÃO FOI POSSÍVEL ENCONTRAR O QUE VOCÊ ESTÁ BUSCANDO.</p>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>
