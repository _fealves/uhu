<?php get_header();the_post()?>

    <section id="single_servico">
        <div class="container-fluid">
            <div class="row">
                <div class="content-single">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-ls-12 col-ls-12">
                                <?php
                                $titulo = get_the_title();
                                $titulo = explode(" ",$titulo);
                                $span = "<span class='frist'>".$titulo[0]."</span>";
                                $titulo[0] = $span;
                                if($titulo[2]){
                                    $span = "<span>".$titulo[1]."</span>";
                                    $titulo[1] = $span;
                                }

                                $title = implode(" ",$titulo);
                                ?>
                                <h1><?php echo $title;?></h1>
                            </div>
                            <div class="col-lg-6 col-md-12 col-ls-12 col-ls-12">
                                <?php

                                $image = get_field('imagem_titulo');

                                if( !empty($image) ): ?>

                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive" />

                                <?php endif; ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p><?php the_content()?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="busca-single">
                    <div class="container">
                        <div class="row buscar">
                            <div class="col-lg-6 col-md-6">
                                <p class="textoBuscar">Precisa de algum serviço em especial ?</p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-ls-12 col-xs-12">
                                <form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform">
                                    <input type="text" name="s" placeholder="Pesquise aqui!"/>
                                    <input type="hidden" name="post_type" value="servico" /> <!-- // hidden 'products' value -->
                                    <input type="image" src="<?php bloginfo('url')?>/wp-content/upload/lupa2.png"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="seu-conforto">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-ls-6">
                                <h2>Seu <span class="azul">conforto</span> <br />é muito <span class="rosa">importante</span> <br />para nós</h2>
                                <p><?php //the_field('seu_conforto')?>
                                    uhu! a gente cuida de você, da sua casa, do seu filho, da sua bagunça e queremos proporcionar o maior conforto e tranquilidade para todos os seus dias.
                                    Conte sempre com os serviços da uhu! e deixa que  a gente cuida ;)
                                </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-ls-6 seu-conforto-img">
                                <img src="<?php bloginfo('url')?>/wp-content/upload/srodki-czystosci-alergia.png" alt="" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>

                <section class="em-breve">
                    <div class="container">
                        <div class="row container-em-breve">
                            <div class="novos col-lg-12">
                                <p>Em breve <br />novos serviços</p>
                            </div>
                            <div class="col-lg-12">
                                <span>Cadastre-se e fique por dentro das novidades <b>Uhu!</b></span>
                            </div>
                            <div class="col-lg-12">
                                <?php echo do_shortcode('[contact-form-7 id="37" title="em-breve"]');?>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </section>

<?php get_footer()?>
