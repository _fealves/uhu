var limpeza;
var organizacao;
var sliderNaMidia;

jQuery(document).ready(function(){
    /* Define o tamanho do height de acordo com a tela*/

    /* Defines header height */
    var windowSize = $(window).height();


    windowSize = (windowSize - 125) - 50;
    $(".banner-sala").height(windowSize);

    /* Change header height on window resize */
    $(window).resize(function(){
        var windowSize = $(window).height();
        windowSize = (windowSize - 125) - 60;
        $(".banner-sala").height(windowSize);


    });

    //Logo fixo home

    /*if($('.home-servicos').length || $('#page-servico').length){
        $(window).scroll(function(){
            $('.linhaDetalhe').css('display','none');
            $('.social').css('display','none');
        });
    }*/

    $(".down_button").click(function(e){
        e.preventDefault();

        var scrollHeight = $(window).height() - $(".menu-principal").height();

        $("html, body").animate({
            scrollTop: scrollHeight
        }, 1000);
    });

    /* Sliders home page */


    /*
     ** Slider serviÃ§os
     */

    /* verifica a largura da tela e adiciona na variavel */
    var largura = $(window).width();

    /* Adiciona a chamada do slider a uma variavel */


    /* Verifica a largura da tela para iniciar o slider */
    if($('.home-servicos').length){
        if (largura < 910) {


            limpeza = $('#slider-limpeza').bxSlider({
                minSlides: 1,
                maxSlides: 1,
                slideWidth: 1000,
                pager: false
            });

            organizacao = $('#slider-organizacao').bxSlider({
                minSlides: 1,
                maxSlides: 1,
                slideWidth: 1000,
                pager: false
            });

            sliderNaMidia = $('.slider-na-midia').bxSlider({
                minSlides: 2,
                maxSlides: 2,
                slideWidth: 2000,
                slideMargin: 5,
                responsive: true,
                pager: false
            });


        } else {


            limpeza = $('#slider-limpeza').bxSlider({
                minSlides: 3,
                maxSlides: 3,
                slideWidth: 1000,
                pager: false
            });

            organizacao = $('#slider-organizacao').bxSlider({
                minSlides: 3,
                maxSlides: 3,
                slideWidth: 1000,
                pager: false
            });

            sliderNaMidia = $('.slider-na-midia').bxSlider({
                minSlides: 4,
                maxSlides: 4,
                slideWidth: 2000,
                slideMargin: 5,
                responsive: true,
                pager: false
            });
        }

        /* Redefine o slider de acordo com o tamanho da tela */

        $(window).resize(function() {
            var largura = $(window).width();

            if (largura < 910) {

                limpeza.reloadSlider({
                    minSlides: 1,
                    maxSlides: 1,
                    slideWidth: 1000,
                    pager: false
                });
                organizacao.reloadSlider({
                    minSlides: 1,
                    maxSlides: 1,
                    slideWidth: 1000,
                    pager: false
                });
                sliderNaMidia.reloadSlider({
                    minSlides: 2,
                    maxSlides: 2,
                    slideWidth: 1000,
                    pager: false
                });


            } else {

                limpeza.reloadSlider({
                    minSlides: 3,
                    maxSlides: 4,
                    slideWidth: 2000,
                    slideMargin: 5,
                    responsive: true,
                    pager: false
                });
                organizacao.reloadSlider({
                    minSlides: 3,
                    maxSlides: 4,
                    slideWidth: 2000,
                    slideMargin: 5,
                    responsive: true,
                    pager: false
                });
                sliderNaMidia.reloadSlider({
                    minSlides: 4,
                    maxSlides: 4,
                    slideWidth: 2000,
                    slideMargin: 5,
                    responsive: true,
                    pager: false
                });


            }

        });
    }

    $('.slider-depoimento').bxSlider({
        minSlides: 1,
        maxSlides: 1,
        //slideWidth: 800,
        slideMargin: 10,
        auto:true
    });


    $(function(){
        $('#tabs_sobre').tabs();
        $( "#accordion" ).accordion({
            collapsible: true,
            active: false,
            heightStyle: "content"
        });
        $( "#accordion-2" ).accordion({
            collapsible: true,
            active: false,
            heightStyle: "content"
        });
    });



});