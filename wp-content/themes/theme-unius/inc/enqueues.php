<?php

/*
 * Include to enqueue scripts for WordPress Unius Theme
 *
 * 
 */

 function theme_unius_styles(){
 	//wp_enqueue_style( 'styles-theme',  get_template_directory_uri() . '/dist/styles/min/main.min.css');
    wp_enqueue_style( 'styles',  get_template_directory_uri() . '/dist/css/style.css');
    wp_enqueue_style( 'styles-bxSlider',  get_template_directory_uri() . '/src/css/jquery.bxslider.css');
    //wp_enqueue_style( 'styles-jqueryui',  get_template_directory_uri() . '/src/css/jquery-ui.min.css');
    wp_enqueue_style( 'styles-bootstrap',  get_template_directory_uri() . '/dist/css/bootstrap.min.css');
    wp_enqueue_style( 'styles-awesome',  get_template_directory_uri() . '/dist/css/font-awesome.min.css');





 }
 add_action( 'wp_enqueue_scripts', 'theme_unius_styles' );

function theme_unius_scripts(){
    wp_enqueue_script('jquery-script', get_template_directory_uri() . '/dist/js/jquery.min.js', '', '');
    wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/src/js/jquery-ui.min.js', '', '',true);
    wp_enqueue_script('bxslider', get_template_directory_uri() . '/src/js/jquery.bxslider.min.js', '', '',true);
    wp_enqueue_script('jquery-bootstrap', get_template_directory_uri() . '/src/js/bootstrap.min.js', '', '');
    wp_enqueue_script('main', get_template_directory_uri() . '/src/js/main.js', '', '',true);
}
add_action( 'wp_enqueue_scripts', 'theme_unius_scripts' );

