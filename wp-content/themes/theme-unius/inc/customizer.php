<?php
/**
*  Theme Customizer File
*  Author: Ciro Valente
*  Description: Using customizer API to change logo from WordPress Unius Theme, 
*  and change basic function from theme without change code.
*  
*/

namespace Theme_Unius\Customizer;

function unius_top_customizer( $wp_customize ) {
	$wp_customize->add_section( 'unius_heading', array(
			'title'	=> esc_html__( 'Headline', 'unius' ),
			'capability' => 'edit_theme_options',
			'description' => esc_html__( 'Change top title and logo', 'unius' ),
			'prioriry' => 120
		)
	);
	$wp_customize->add_setting( 'unius_headline', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
			'transport' => 'postMessage'
		)
	);
	$wp_customize->add_control( 'unius_headline_control', array(
			'label' => esc_html__( 'Headline ', 'unius' ),
			'section' => 'unius_heading',
			'settings' => 'unius_headline',
			'type' => 'text'
		)
	);
	$wp_customize->add_setting( 'unius_heading_text', array(
			'type'          => 'theme_mod', //
			'capability'    => 'edit_theme_options',
			'transport'     => 'postMessage'
		)
	);
	$wp_customize->add_control( 'unius_heading_text_control', array(
			'label'         => esc_html__( 'Heading Text: ', 'unius' ),
			'section'       => 'unius_heading',
			'type'          => 'text',
			'settings'      => 'unius_heading_text'
		)
	);

}
add_action( 'customize_register' , __NAMESPACE__ .  '\\unius_top_customizer' );
