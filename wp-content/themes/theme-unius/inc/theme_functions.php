<?php 

/* Disable WordPress Admin Bar for all users but admins. */

show_admin_bar(false);

add_action( 'after_setup_theme', 'register_theme_menu' );
function register_theme_menu() {
    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'Top'   => __( 'Top primary menu', 'theme_unius' ),
        'Footer-Right' => __( 'Right menu in footer', 'theme_unius' ),
        'principal-footer' => __( 'Left menu in footer', 'theme_unius' ),
        'footer' => __( 'menu in footer', 'theme_unius' ),

    ) );

}

function template_chooser($template)
{
    global $wp_query;
    $post_type = get_query_var('post_type');
    if( $wp_query->is_search && $post_type == 'servico' )
    {
        return locate_template('archive-search.php');  //  redirect to archive-search.php
    }
    return $template;
}
add_filter('template_include', 'template_chooser');