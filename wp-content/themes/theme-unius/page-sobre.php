<?php get_header(); ?>

<div class="container-fluid" id="pagina-sobre">

    <section id="sobre">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 title-sobre">
                        <h1>Sobre Nós</h1>
                        <p>O cuidado que você precisa pra deixar sua casa sempre limpa e organizada</p>
                    </div>
                    <div id="tabs_sobre">
                        <ul class="col-lg-12">
                            <li class="col-lg-4 col-md-4 col-ls-4 col-xs-4"><a href="#missao">Nossa missão</a></li>
                            <li class="col-lg-4 col-md-4 col-ls-4 col-xs-4"><a href="#visao">Nossa visão</a></li>
                            <li class="col-lg-4 col-md-4 col-ls-4 col-xs-4"><a href="#valores">Nossos valores</a></li>
                        </ul>
                        <div class="col-lg-12" id="missao">
                            <p>
                                <?php the_field('missao')?>
                            </p>
                        </div>

                        <div class="col-lg-12" id="visao">
                            <p>
                                <?php the_field('visao')?>
                            </p>
                        </div>

                        <div class="col-lg-12" id="valores">
                            <p>
                                <?php the_field('valores')?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="content-sobre">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4 col-md-4 col-ls-4 col-xs-12">
                            <div class="position cuida">
                                <h3>A gente cuida</h3>
                                <p>Conte com a uhu! pra cuidar dos seus filhos e animais.</p>
                                <a>Saiba mais</a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-ls-4 col-xs-12">
                            <div class="position limpa">
                                <h3>A gente limpa</h3>
                                <p>Conte com a uhu! pra limpeza da sua casa.</p>
                                <a>Saiba mais</a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-ls-4 col-xs-12">
                            <div class="position organiza">
                                <h3>A gente organiza</h3>
                                <p>Conte com a uhu! pra organizar o seu lar.</p>
                                <a>Saiba mais</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="faq">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-ls-2 col-xs-12 img-destaque">
                        <img src="<?php bloginfo('url')?>/wp-content/upload/uhu.jpg">
                    </div>

                    <div class="col-lg-10 col-md-10 col-ls-10    col-xs-12">
                        <h2>FAQ</h2>
                        <p>Confira algumas dúvidas frequentes de nossos clientes. Quer mandar a sua dúvida também? Escreva para:</p>

                            <p>contato@uhuagentecuida.com.br</p>
                            <p>duvidas@uhuagentecuida.com.br</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 accordions">

                        <div class="col-lg-6">
                            <div id="accordion">
                                <h3>A alimentação e o transporte estão inclusos no valor do contrato?</h3>
                                <p>A alimentação sim, mas o transporte é dado diretamente para a nossa colaboradora.
                                </p>

                                <h3>Contratando o serviço "Limpeza de manutenção", posso mudar na hora para "Limpeza pesada"?</h3>
                                <p>Havendo disponibilidade no horário das nossas colaboradoras, certamente podemos alterar o serviço.
                                </p>

                                <h3>Posso passar e limpar ao mesmo tempo?</h3>
                                <p>O serviço de limpeza e de passadoria envolvem profissionais diferentes.
                                    Podemos marcar os dois serviços para o mesmo dia, mas não com a mesma pessoa.
                                </p>

                                <h3>Como é feito o pagamento?</h3>
                                <p>Após o fechamento do contrato do serviço, disponibilizamos o boleto bancário.
                                </p>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div id="accordion-2">
                                <h3>Caso eu precise, como é feito o cancelamento ou reagendamento?</h3>
                                <p>De acordo com o contrato, reagendamos o serviço desde que nos seja avisado com antecedência mínima de 24h.
                                    No caso de cancelamento, a  devolução do pagamento será feita com os descontos dos impostos.
                                </p>


                                <h3>Considero-me chata com limpeza. O que me faria contratá-los?</h3>
                                <p>A confiabilidade na nossa proposta de trabalho, que é cuidar da sua casa como a nossa casa.
                                </p>

                                <h3>Suas profissionais são qualificadas?</h3>
                                <p>Todas as nossas colaboradoras passam por um treinamento efetivo onde aprendem a deixar a sua casa limpa e organizada do jeito que você tanto aprecia.
                                </p>


                                <h3>Quais os horários vocês atendem?</h3>
                                <p>Iniciamos as nossas atividades às 7h e temos equipes disponíveis até às 22h.
                                    O tempo de duração de uma limpeza dependerá do que for contratado e do tamanho do imóvel.
                                </p>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="depoimentos">
        <div class="row">
            <?php include(locate_template('template-parts/_block_depoimentos.php'));?>
        </div>
    </section>
</div>

<?php get_footer();?>
