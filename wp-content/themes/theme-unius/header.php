<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Theme_Unius
 * @since Theme Unius 1.0
 */
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<title><?php wp_title(''); ?></title>
	<?php wp_head(); ?>
</head>
<body>
<div class="container-fluid header-title">

<?php if(is_home() || is_page('servicos')){?>
	<div id="home">
		<div class="row top-logo">
			<nav class="col-lg-12  logo-fixa">
				<div class="logo">
					<?php if(is_home()){ ?>
					<h1><a class="logoDetalhe" href="<?php bloginfo('url')?>"><img src="<?php bloginfo('url')?>/wp-content/upload/logoPrincipal.jpg" alt="Uhu!" title="logo"></a></h1>
					<?php }else{ ?>
					<h2><a class="logoDetalhe" href="<?php bloginfo('url')?>"><img src="<?php bloginfo('url')?>/wp-content/upload/logoPrincipal.jpg" alt="Uhu!" title="logo"></a></h2>
					<?php }?>
					<span class="linhaDetalhe"></span>
					<ul class="social">
						<li><a href="https://www.facebook.com/Uhu-a-gente-cuida-962838803799629/" target="_blank"><img src="/wp-content/upload/facebook.png" alt="Facebook" title="Facebook"></a></li>
						<li><a href="https://www.linkedin.com/company/10560060?trk=tyah&trkInfo=clickedVertical%3Acompany%2CentityType%3AentityHistoryName%2CclickedEntityId%3Acompany_10560060%2Cidx%3A0" target="_blank"><img src="/wp-content/upload/linkdin.png" alt="Linkdin" title="Linkdin"></a></li>
						<li><a href="https://www.instagram.com/uhu_agentecuida/" target="_blank"><img src="/wp-content/upload/insta.png" alt="Instagram" title="Instagram"></a></li>
						<li><a href="https://twitter.com/uhu_agentecuida" target="_blank"><img src="/wp-content/upload/twitter.png" alt="twitter" title="twitter"></a></li>
					</ul>
				</div>
			</nav>
		</div>
			<!--Banner sala-->
		<?php if(is_home()){?>
		<div class="row banner-sala">
		<?php }else{?>
			<div class="row banner-sala banner-limpeza">
		<?php }?>
			<div class="faixa-top">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-ls-6 col-xs-12"><p class="textFaixa">Seu conforto é</p> <p class="textFaixa">muito importante para nós!</p></div>
						<div class="col-lg-5 col-md-5 col-lg-offset-1 col-md-offset-1 col-ls-4 col-ls-offset-2 col-xs-12 boxBranca">
							<div class="contato">
								<p class="numeroTop"><span class="prefixo">+55</span> 21 9999-9999</p>
								<p><span class="prefixo">+55</span> 21 9999-9999</p>
							</div>
							<p class="titleBoxBranca"><span>Limpeza Total</span></p>
							<p class="textBoxBranca">Está precisando de uma ajuda especial com a sua casa? uhu! A gente cuida. Você vai se surpreender!</p>
						</div>
						<!--<div class="col-md-12">
							<button class="down_button">
								<i class="pulse"></i>
							</button>
						</div>-->
					</div>
				</div>
			</div>

		</div>
		<!--<div class="row menu-principal">
			<div class="container">
				<?php
/*				wp_nav_menu(array(
					'theme_location' => 'Top',
					'container' => 'div',
					'container_class' => 'collapse navbar-collaps',
					'container_id' => 'main-navbar-collapse',
					'menu_id' => '',
					'echo' => true,
					'fallback_cb' => 'wp_page_menu',
					'before' => '',
					'after' => '',
					'link_before' => '',
					'link_after' => '',
					'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth' => 0,
					'walker' => ''
				));
				*/?>
			</div>
		</div>-->
	</div>
<?php }else{ ?>

	<div class="row top-logo-pages">
		<div class="col-lg-12">
			<div class="logo">
				<h2><a class="logoDetalhe" href="<?php bloginfo('url')?>"><img src="<?php bloginfo('url')?>/wp-content/upload/logoPrincipal.jpg" alt="Uhu!" title="logo"></a></h2>
				<span class="linhaDetalhe"></span>
				<ul class="social">
					<li><a href=""><img src="/wp-content/upload/facebook.png" alt="" title=""></a></li>
					<li><a href=""><img src="/wp-content/upload/linkdin.png" alt="" title=""></a></li>
					<li><a href=""><img src="/wp-content/upload/insta.png" alt="" title=""></a></li>
					<li><a href=""><img src="/wp-content/upload/twitter.png" alt="" title=""></a></li>
				</ul>
			</div>
		</div>
	</div>

<?php }?>
	<div class="row">
		<nav class="navbar menu-principal">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Top" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse menu" id="Top">

					<?php

					wp_nav_menu( array(
						'theme_location' 	=> 'Top',
						'container' 		=> false,
						'container_class' 	=> 'collapse navbar-collapse',
						'container_id'    	=> 'bs-example-navbar-collapse-1 ',
						'menu_class'      	=> 'nav navbar-nav',
						'menu_id'         	=> '',
						'echo'            	=> true,
						'fallback_cb'     	=> 'wp_page_menu',
						'before'          	=> '',
						'after'           	=> '',
						'link_before'     	=> '',
						'link_after'      	=> '',
						'items_wrap'      	=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           	=> 0,
						'walker'          	=> ''
					));
					?>
					<?php if(is_home() || is_page('servicos')){?>
					<div class="orcamento">
						<a href="/orcamento">Solicite um orçamento</a>
					</div>
					<?php }else{?>
						<div class="orcamento orcamento-interna">
							<a href="/orcamento">Solicite um orçamento</a>
						</div>
					<?php }?>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
	</div>
</div>

<?php if(!is_front_page()): ?>
	<div class="container breadcrumb-header">
		<div class="row">
			<div class="col-lg-12 container-breadcrumb">
				<?php if ( function_exists('yoast_breadcrumb') && !is_front_page() ) : ?>
					<div class="row breadcrumb-row">
						<div class="col-xs-12">
							<?php yoast_breadcrumb('<p id="breadcrumbs">','</p>'); ?>
						</div>
					</div>
				<?php endif; ?>

			</div>
		</div>
	</div>
<?php endif; ?>
