		<?php
/**
 * The template for displaying Home
 *
 * This is the template that display Home.
 *
 * @package WordPress
 * @subpackage Theme_Unius
 * @since Theme Unius 1.0
 */

get_header();?>
<div class="container-fluid">
	<!--Busca Serviços-->

	<div class="row">
		<?php include(locate_template('template-parts/_block_pesquise.php'));?>
	</div>

	<!--Serviços-->

	<div class="row home-servicos">
		<?php include(locate_template('template-parts/_block_servicos.php'));?>
	</div>

	<!-- Em Breve -->

	<div class="row em-breve">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="center-embreve">
						<p>Em breve <br> novos serviços</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Destaque -->
	<div class="row">
		<div class="container destaque">
			<div class="col-lg-12">
				<div class="col-lg-2 col-md-2 img-destaque">
					<img src="<?php bloginfo('url')?>/wp-content/upload/uhu.jpg">
				</div>
				<div class="texto-destaque col-lg-10 col-md-10 ">
					<h2><!--Lorem Ipsum Dolor Sit--></h2>
					<p>uhu! Finalmente você encontrou a empresa que você precisava pra deixar sua casa sempre limpa e organizada!</p>
					<p>Com método tradicional de limpeza brasileira, a uhu! usa água, sabão e esfrega mesmo pra deixar sua casa sempre brilhando!</p>
					<p>Agora você tem com quem contar na hora que precisar de uma faxina. Entre em contato com a uhu! e se surpreenda com os resultados de nossos serviços.</p>
					<p>Receba em sua casa nossas colaboradoras treinadas e preparadas para uma limpeza ágil e eficaz.</p>
					<p>Conte sempre com a uhu! para limpezas pesadas, manutenções e organização de casas e apartamentos!</p>
				</div>
			</div>
			<div class="texto-destaque-2 col-lg-12">
				<p class="destaque-center">Sua casa é a nossa casa, <span class="destaque-rosa">a gente cuida!</span> ;)</p>
			</div>
		</div>
	</div>

	<!-- Depoimentos-->
	<div class="row">
		<?php include(locate_template('template-parts/_block_depoimentos.php'));?>
	</div>

	<!-- Na midia-->
	<!--<div class="row na-midia">
		<?php /*include(locate_template('template-parts/_block_na_midia.php'));*/?>
	</div>-->
</div>
<?php get_footer(); ?>

