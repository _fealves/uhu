<?php
/* Template Name: Custom Search */
get_header(); ?>


    <section id="page-servico">
        <div class="container-fluid">
            <div class="row">
                <!--<section id="conteudo-servico">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 title-servico">
                                <h1><?php /*the_title();*/?></h1>
                                <p>Aliquam dapibus massa diam, eu porta ipsum mollis vel. Etiam blandit auctor porta.</p>
                            </div>
                            <div class="col-lg-12 content-sobre">
                                <p><?php /*the_content();*/?></p>
                            </div>
                        </div>
                    </div>
                </section>-->

                <section id="organizacao">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Organização</h2>
                            </div>
                            <div class="col-lg-12 organizacao">
                                <?php
                                $limpeza = get_term_by('slug','organizacao','servicos');
                                $categoria = get_term($limpeza,'servicos');


                                $args = array(
                                    'post_type'  => 'servico',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy'          => 'servicos',
                                            'field'             => 'term_id',
                                            'terms'             => $categoria->term_id
                                        ),
                                    ),
                                );

                                $query = new WP_Query($args);
                                echo $cat->name;
                                if($query->have_posts()){
                                    while($query->have_posts()){
                                        $query->the_post();
                                        ?>
                                        <div class="col-lg-4 col-md-4 col-ls-4 col-xs-12  item-organizacao">
                                            <a href="<?php echo get_permalink($query->ID)?>" class="hover-limpeza">
                                                <img width="230" height="165" src="<?php the_field('img_principal')?>" alt="" style="" class="img-principal">
                                                <img width="230" height="165" src="<?php the_field('img_hover')?>" alt="" class="img-hover">
                                            </a>
                                        </div>
                                        <?php
                                    }
                                }

                                ?>

                            </div>
                        </div>
                    </div>
                </section>

                <section class="em-breve">
                    <div class="container">
                        <div class="row container-em-breve">
                            <div class="novos col-lg-12">
                                <p>Em breve <br />novos serviços</p>
                            </div>
                            <div class="col-lg-12">
                                <span>Cadastre-se e fique por dentro das novidades <b>uhu!</b></span>
                            </div>
                            <div class="col-lg-12">
                                <?php echo do_shortcode('[contact-form-7 id="37" title="em-breve"]');?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>


<?php get_footer(); ?>