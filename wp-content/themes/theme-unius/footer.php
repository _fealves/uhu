<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Theme_Unius
 * @since Theme Unius 1.0
 */
?>

<footer id="colophon" class="site-footer container" role="contentinfo">
	<div class="row footer-uhu">
		<div class="col-xs-12 col-sm-8 site-footer__first-container">
			<div class="col-xs-12 col-sm-4 footer-menu">
				<h5 class="footer-menu__title">Uhu!</h5>

				<ul class="footer-menu__list">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'principal-footer',
						'container' => '',
						'echo' => true,
						'before' => '',
						'after' => '',
						'link_before' => '',
						'link_after' => '',
						'items_wrap' => '%3$s',
						'depth' => 0,
						'walker' => ''
					));
					?>
				</ul>
			</div>

			<div class="col-xs-12 col-sm-3 footer-menu">
				<h5 class="footer-menu__title">Serviços</h5>

				<ul class="footer-menu__list">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'footer',
						'container' => '',
						'echo' => true,
						'before' => '',
						'after' => '',
						'link_before' => '',
						'link_after' => '',
						'items_wrap' => '%3$s',
						'depth' => 0,
						'walker' => ''
					));
					?>
				</ul>
			</div>

			<div class="col-xs-12 col-sm-5 footer-menu">
				<h5 class="footer-menu__title">Contato</h5>

				<ul class="footer-menu__list">
					<!--<li>Rua Lorem Ipsum, 2365</li>
					<li>Rio de Janeiro - RJ</li>
					<li>CEP: 00000-000</li>-->
					<li><span class="footer-menu_tel">+55 21 9999-9999</span></li>
					<li><span class="footer-menu_tel">+55 21 9999-9999</span></li>
					<li><span class="footer-menu_tel">+55 21 9999-9999</span></li>
					<li><a href="mailto:">email@uhuagentecuida.com.br</a></li>
				</ul>
			</div>
		</div>

		<div class="col-xs-12 col-sm-4 footer-menu">
			<h5 class="footer-menu__title newslatter">Inscreva-se em nossa Newsletter</h5>

			<div class="footer-menu__form col-sm-12 col-xs-12">
				<?php echo do_shortcode("[contact-form-7 id=\"51\" title=\"newslatter\"]")?>
			</div>

			<div class="footer-menu__logo-text col-sm-12 col-xs-12">
				<span>uhu! A gente cuida</span>
			</div>
		</div>
	</div>

	<div class="site-info row">
		<span class="site-info__text col-xs-12">Desenvolvido por <a href="https://www.unius.com.br/">UNIUS</a></span>
	</div><!-- .site-info -->

</footer>




<?php wp_footer(); ?>

</body>
</html>