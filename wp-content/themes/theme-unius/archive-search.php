<?php
/* Template Name: Custom Search */
get_header(); ?>


    <section id="search">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Resultado da busca : <?php echo "$s"; ?></h3>
                </div>
                <div class="col-lg-12 content-search">

                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="col-lg-4 item-search">
                        <a href="<?php the_permalink(); ?>">

                            <img width="230" height="165" src="<?php the_field('img_principal')?>" alt="" style="" class="img-principal">

                        </a>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>



<?php get_footer(); ?>