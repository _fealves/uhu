<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'uhul');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'aO|jGr$S6=LeuF+?*7:skPLF%&Pjl$|cGvqSaZ}0#_1>O}O@R>o}1c2#q-rcF40O');
define('SECURE_AUTH_KEY',  'rRYZkof%~?8sN8{)0{K1<YkD1W])Ay^:j__iYkyXp2AQ=:BU3/_M49ZI5;u7Bm`^');
define('LOGGED_IN_KEY',    '-T>!u[#`+9,3~@|Vr4eVb2_a;h#}|$+m6g/,Ld?rs5o%%Ynp{O{)+NeQ/{I$|ZkQ');
define('NONCE_KEY',        'vB)=Ae^|2f,|}OXUj5g Jw{OF[d jz<Yr`5.Y`OaKC3fjF:sA&@^d(U|Pd-f}G}+');
define('AUTH_SALT',        'l-YU(nPxa2y:dxm_MBX8tSDR$5IRSBp->_K4BKz70HSH&3Y,k0wbl &{Cm+|q*:Q');
define('SECURE_AUTH_SALT', ' }G1VW%qlD$cm)k8+RoT:i<6ZniBdXST@zZ:msS~.]~tE/AEf|yH0U#>3VA-5w&a');
define('LOGGED_IN_SALT',   '?ksgev8rq,n2{^Xc<eMsA{;[}wHRc*hh:b1t*q6bcyoqys-atIQ@Kfgk@cx;:`Rs');
define('NONCE_SALT',       'i[c2N0@%Pk|b,S+HO=Hj+`S!B+]96sb+)1/jC +sD:Lc?XMWd>T?B$RH5*JaiR,F');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
